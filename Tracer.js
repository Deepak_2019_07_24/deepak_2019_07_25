var Tracer = function(){
	this.x = 0;
	this.y = 0;
	this.xDiff = 0;
	this.event = null;
	this.isDown = false;
	this.isClicked = false;
	this.uiObj;
	var thisRef = this;
	var ref = {};
	this.tracer;
	this.canvas;
	this.tracerTarget;
	this.init = function(){
	
		this.canvas = document.createElement("canvas");
		$(this.canvas).attr('id',"tracerCanvas");
			$("#tracerCntr").append(this.canvas);
		this.tracer = new fabric.Canvas(this.canvas,{
			width:600,
			height:550
		});
		
		var rect = new fabric.Rect({
		  left: 100,
		  top: 100,
		  fill: 'red',
		  width: 20,
		  height: 20
		});
		//this.tracer.renderAll();
		//this.tracer.add(rect);
		
		this.enableTracer();
	};
	this.getTracer = function(){
		return this.tracer;
	};
	this.setTracerTarget = function(pTarget){
		this.tracerTarget = pTarget;
	};
	this.getTracerTarget = function(){
		return this.tracerTarget;
	};
	this.addObject = function(pObj){
		if(pObj){
			pObj.canvas = thisRef.tracer;
			thisRef.tracer.add(pObj);
			thisRef.tracer.setActiveObject(pObj);
		}
	};
	this.removeObject = function(){
		if(thisRef.tracer.getActiveObject()){
			thisRef.tracer.remove(thisRef.tracer.getActiveObject());
			thisRef.tracer.renderAll();
		}
	};
	this.removeAllObjects = function(){
		thisRef.tracer.forEachObject(function(o){
			if(o){
				o.remove();
			}
		});
	};
	this.enableTracer = function(){
		this.tracer.on('mouse:move',thisRef.onTracerMove);
		this.tracer.on('mouse:down',thisRef.onTracerDown);
		this.tracer.on('mouse:up',thisRef.onTracerUp);
		this.tracer.on("object:moving", thisRef.onTracerObjMove);
	};
	this.disableTracer = function(){
		$("#tracer").off('mousemove');
		$("#tracer").off('click');
		$("#tracer").off('mousedown');
		$("#tracer").off('mouseup');
	};
	this.getX = function(){
		return  thisRef.x ;
	};
	this.getY = function(){
		return thisRef.y;
	};
	this.getXY = function(){
		var xyObj = {
			"x" : thisRef.x,
			"y" : thisRef.y
		};
		return xyObj;
	}
	this.onTracerMove = function(evt){
		
		thisRef.event = "mousemove";
		thisRef.x = evt.e.clientX;
		thisRef.y = evt.e.clientY;
		ref.mousemove ? ref.mousemove(evt): null;
		var movedObject = thisRef.tracer.getActiveObject();
		console.log(movedObject)
		if (movedObject && movedObject !== null) {
			movedObject.left = (evt.e.clientX - movedObject.width / 2);
		  movedObject.top = (evt.e.clientY - movedObject.height / 2);
		  movedObject.setCoords();
		  thisRef.tracer.renderAll();
		}
	};
	this.updateXYObj = function(){
		if(thisRef.tracer.getActiveObject()){
			var actObj = thisRef.tracer.getActiveObject();
			var coords = actObj.calcCoords();
			coords.tl.x= thisRef.x;
			coords.tl.y= thisRef.y;
			thisRef.tracer.renderAll();
		//	console.log("onTracerMove",thisRef.tracer.getActiveObject());
		}
	}
	this.onTracerObjMove = function(evt){
		console.log("onTracerObjMove");		
		//sthisRef.updateXYObj();
	};
	this.onTracerClick = function(evt){
		thisRef.event = "click";
		//console.log(evt.pageX - thisRef.xDiff + " :onTracerClick: "+ evt.pageY);
		thisRef.x = evt.e.clientX;
		thisRef.y =	evt.e.clientY;
		ref.click ? ref.click(): null;
	};
	this.onTracerDown = function(evt){
		thisRef.event = "mousedown";
		thisRef.isDown = true;
		thisRef.x = evt.e.clientX;
		thisRef.y = evt.e.clientY;
		ref.mousedown ? ref.mousedown(evt): null;
		
	};
	this.onTracerUp = function(evt){
		if(evt && evt.target){
		var movedObject = thisRef.tracer.getActiveObject();
		console.log(movedObject)
		if (movedObject && movedObject !== null) {
			movedObject.left = 0;
		  movedObject.top = 0;
		  movedObject.setCoords();
		  thisRef.tracer.renderAll();
		}
			thisRef.setTracerTarget(evt.target);
		}
		thisRef.event = "mouseup";
		thisRef.isDown = false;
		thisRef.x = evt.e.clientX;
		thisRef.y = evt.e.clientY;
		ref.mouseup ? ref.mouseup(evt): null;
	};
	this.addEventListener = function(pEvt, pFun)
	{
		ref[pEvt] = pFun;
	};
	this.removeEventListener = function(pEvt)
	{
		delete ref[pEvt];
	}
}
