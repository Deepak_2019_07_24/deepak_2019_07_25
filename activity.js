var AssignMent = function(){
	this.data;	
	this.isMouseDown = false;
	this.activeObject;
	this.selectedCanvas;
	this.sourceCanvas;
	this.targetCanvas;
	this.currentDDCnv;
	this.tracerRef;
	var thisRef = this;
	var totalCnvs;
	var canvasObj = [];
	
	var isTouchDevice = false;
	this.init = function(){
		$("#genRndCnv").off('click').on('click',this.createCanvas);
		this.tracerRef = new Tracer();
		this.tracerRef.init();
	};
	
	this.createCanvas = function(c){
		totalCnvs = Math.floor(getRandomN(2,5));
		$("#canvasView").show();
		$("#cnvList").empty();
		canvasObj = [];
		$("#cnvsContainer").html('');
		thisRef.tracerRef.removeAllObjects();
		for(var i=0;i<totalCnvs;i++){
			var cnv = document.createElement("canvas");
			$(cnv).addClass('cnvs');
			$(cnv).attr('id',"canvas_"+i);
			$("#cnvsContainer").append(cnv);
			var cnvTemp = {};			
			var canvas = new fabric.Canvas("canvas_"+i, {
				width:600
			});
			cnvTemp["canvas_"+i] = {};
			cnvTemp["canvas_"+i]["canvas"] = canvas;
			canvasObj.push(cnvTemp);			
			thisRef.addCanvasListMenu("canvas_"+i);
			canvas.renderAll();
		}		
		//console.log(canvasObj);
		thisRef.addEventOnCanvas();
		thisRef.addEventOnCnvList();
		$("#inrtToCnv").off('click').on('click',thisRef.onInrtToCnv);
		thisRef.currentDDCnv =  $("#cnvList").find(':selected').val();
	};
	this.addCanvasListMenu = function(pOpt){
		$("#cnvList").append($('<option></option>').val(pOpt).html(pOpt));
	};
	this.addEventOnCanvas = function(){
		for(var i=0;i<canvasObj.length;i++){
			var cnvs = canvasObj[i]["canvas_"+i]['canvas'];
			cnvs.on('mouse:move',thisRef.onCnvMove);
			cnvs.on('mouse:down',thisRef.onCnvDown);
			cnvs.on('mouse:up',thisRef.onCnvUp);
			cnvs.on("object:moving", thisRef.onObjectMoving);
		}
		
		thisRef.tracerRef.addEventListener("mousedown", thisRef.onMouseEvent);
		thisRef.tracerRef.addEventListener("mouseup", thisRef.onMouseEvent);
		thisRef.tracerRef.addEventListener("mousemove", thisRef.onMouseEvent);
	};
	this.addEventOnCnvList = function(){
		$("#cnvList").off('change').on('change',function(e){
			thisRef.currentDDCnv =  $(this).find(':selected').val();
		});
	};
	this.onInrtToCnv = function(){
		thisRef.getUserObj();
		thisRef.drawObjects();
	};
	this.getUserObj = function(){
		var lCnvIndx = thisRef.currentDDCnv.split("_")[1];
		var lCanvas = canvasObj[lCnvIndx]["canvas_"+lCnvIndx];
		lCanvas['userObj'] = [];
		lCanvas["drawObj"] = [];
		lCanvas['userObj'].push(Refdata[0]);
		lCanvas['userObj'].push(Refdata[1]);
		lCanvas['userObj'].push(Refdata[Refdata.length-1]);
		lCanvas['userObj'].push(Refdata[Refdata.length-2]);
		lCanvas['userObj'].push(Refdata[Math.floor(getRandomN(2,Refdata.length-3))]);
	};
	
	this.drawObjects = function(){
		var lCnvIndx = thisRef.currentDDCnv.split("_")[1];
		console.log(canvasObj[lCnvIndx]);
		var lUserObj = canvasObj[lCnvIndx]["canvas_"+lCnvIndx]['userObj'];
		var lDrawObj = canvasObj[lCnvIndx]["canvas_"+lCnvIndx]['drawObj'];
		var lCanvas = canvasObj[lCnvIndx]["canvas_"+lCnvIndx]['canvas'];
		for(var i=0;i<lUserObj.length;i++){
			var lId = lUserObj[i].id;
			var lAlbumId = lUserObj[i].albumId;
			var lTitle = lUserObj[i].title;
			var lUrl = lUserObj[i].url;
			var lThumbnailUrl = lUserObj[i].thumbnailUrl;			
			// get object for draw
			var lObj;
			if(lAlbumId >= 100){
			
			lObj = thisRef.draw(lUrl,"text",lCanvas);
			}else{
				if(lId%2 == 1){
					lObj = thisRef.draw(lThumbnailUrl,"image",lCanvas);
				}else{
					lObj = thisRef.draw(lTitle,"text",lCanvas);
				}
			}
			lDrawObj.push(lObj);
		}
	};
	this.draw = function(pRef,pType,pCanvas){
		var lObj;
		if(pType == "text"){
			lObj = new fabric.Text(pRef, {
			  fontFamily: 'Comic Sans',
			  fontSize: 14
			});
			pCanvas.add(lObj);
		}
		if(pType == "image"){			
			fabric.Image.fromURL(pRef, function(oImg) {
				pCanvas.add(oImg);
			});
		}
	};
	
	this.onCnvMove = function(e) {
		/*if(thisRef.isMouseDown) {	
			console.log("onCnvMove",e);
			if(thisRef.tracerRef.getTracerTarget()){
				var actObj = thisRef.tracerRef.getTracerTarget();
				var coords = actObj.oCoords;
				coords.tl.x= thisRef.tracerRef.x;
				coords.tl.y= thisRef.tracerRef.y;
				thisRef.tracerRef.renderAll();
			}	
		}*/
	};
	this.onCnvDown = function(e){
		thisRef.isMouseDown = true;
		if(e.target && e.target.canvas) {	
			if(e.target.canvas.getActiveObject()){
				migrateItem(e.target.canvas,thisRef.tracerRef.getTracer(),e.target);
			}	
		}
	};
	
	this.onCnvUp = function(e){		
		thisRef.isMouseDown = false;
		if(thisRef.tracerRef.getTracerTarget()){
			var cnvsId = thisRef.selectedCanvas.lowerCanvasEl.id;
			var lCnvIndx = cnvsId.split('_')[1]; 
			var lCanvas = canvasObj[lCnvIndx]["canvas_"+lCnvIndx]['canvas'];
			migrateItem(thisRef.tracerRef.getTracerTarget().canvas,lCanvas,thisRef.tracerRef.getTracerTarget());
		}
	};
	this.onMouseEvent = function(e){
		//alert("onClick");
		thisRef.getUIElement(e);
	};
	this.getUIElement = function(e){
		var lx = thisRef.tracerRef.getXY().x;
		var ly = thisRef.tracerRef.getXY().y;
		//console.log(lx +" :: "+ ly);
		for(var i=0;i<canvasObj.length;i++){
			var cnvs = canvasObj[i]["canvas_"+i]['canvas'];
			var c_left = cnvs._offset.left;
			var c_top = cnvs._offset.top;
			var c_Id;
			if(lx >= c_left && ly >= c_top && lx <= (c_left+600) && ly <= (c_top + 150)){	
				addLog(cnvs.lowerCanvasEl.id)	
				thisRef.selectedCanvas = cnvs;
				if(thisRef.tracerRef.event){
					 var evt = new MouseEvent(thisRef.tracerRef.event, {
						clientX: lx,
						clientY: ly,
						canvas:cnvs,
						activeObject : cnvs.getActiveObject()
					  });
					//  console.log(thisRef.tracerRef.event);
					if(cnvs){
						//thisRef.activeObject = cnvs.getActiveObject();
						cnvs.upperCanvasEl.dispatchEvent(evt);
					}
				}
			}
		}
	};
	this.onObjectMoving = function(p) {
		console.log("onObjectMoving",p);
	};
	
	window.addEventListener("touchstart", function() {
		isTouchDevice = true;
	});
	var migrateItem = function(fromCanvas, toCanvas, pendingImage) {
		fromCanvas.remove(pendingImage);

		var pendingTransform = fromCanvas._currentTransform;
		fromCanvas._currentTransform = null;

		var removeListener = fabric.util.removeListener;
		var addListener = fabric.util.addListener; 
		{
			removeListener(fabric.document, 'mouseup', fromCanvas._onMouseUp);
			removeListener(fabric.document, 'touchend', fromCanvas._onMouseUp);

			removeListener(fabric.document, 'mousemove', fromCanvas._onMouseMove);
			removeListener(fabric.document, 'touchmove', fromCanvas._onMouseMove);

			addListener(fromCanvas.upperCanvasEl, 'mousemove', fromCanvas._onMouseMove);
			addListener(fromCanvas.upperCanvasEl, 'touchmove', fromCanvas._onMouseMove, {
				passive: false
			});

			if (isTouchDevice) {
				var _this = fromCanvas;
				setTimeout(function() {
					addListener(_this.upperCanvasEl, 'mousedown', _this._onMouseDown);
				}, 500);
			}
		} 
		{
			addListener(fabric.document, 'touchend', toCanvas._onMouseUp, {
				passive: false
			});
			addListener(fabric.document, 'touchmove', toCanvas._onMouseMove, {
				passive: false
			});

			removeListener(toCanvas.upperCanvasEl, 'move', toCanvas._onMouseMove);
			removeListener(toCanvas.upperCanvasEl, 'touchmove', toCanvas._onMouseMove);

			if (isTouchDevice) {
				// Unbind mousedown to prevent double triggers from touch devices
				removeListener(toCanvas.upperCanvasEl, 'mousedown', toCanvas._onMouseDown);
			} else {
				addListener(fabric.document, 'mouseup', toCanvas._onMouseUp);
				addListener(fabric.document, 'mousemove', toCanvas._onMouseMove);
			}
		}

		setTimeout(function() {
			//pendingImage.scaleX *= -1;
			pendingImage.canvas = toCanvas;
			//pendingImage.migrated = true;
			toCanvas.add(pendingImage);

			toCanvas._currentTransform = pendingTransform;
			//toCanvas._currentTransform.scaleX *= -1;
			//toCanvas._currentTransform.original.scaleX *= -1;
			toCanvas.setActiveObject(pendingImage);
			toCanvas.renderAll();
		}, 5);
	};

	function addLog(msg){
		$("#console").text("\n"+msg);
	}
	this.loadData = function(){
		$.getJSON( "photos.json", function( data ) {
		  thisRef.data = data;
		  console.log(thisRef.data);
		});
	};
	function getRandomN(min, max){
		 return Math.random() * (max - min) + min;
	}
}
